/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.apphelpu.Model;

/**
 *
 * @author apalencia
 */
public class AsignarSim {
    
    private String identificacion;
    private String pto_venta;
    private String puerto;
    private String obs;
    private String usuario;
    private String nombre;
    private int usuariosis;

    public AsignarSim(String identificacion, String pto_venta, String puerto, String obs, String usuario, String nombre, int usuariosis) {
        this.identificacion = identificacion;
        this.pto_venta = pto_venta;
        this.puerto = puerto;
        this.obs = obs;
        this.usuario = usuario;
        this.nombre = nombre;
        this.usuariosis = usuariosis;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getPto_venta() {
        return pto_venta;
    }

    public void setPto_venta(String pto_venta) {
        this.pto_venta = pto_venta;
    }

    public String getPuerto() {
        return puerto;
    }

    public void setPuerto(String puerto) {
        this.puerto = puerto;
    }

    public String getObs() {
        return obs;
    }

    public void setObs(String obs) {
        this.obs = obs;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getUsuariosis() {
        return usuariosis;
    }

    public void setUsuariosis(int usuariosis) {
        this.usuariosis = usuariosis;
    }
    
    
    
}
