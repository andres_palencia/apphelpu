/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.apphelpu.Model;

import java.sql.Date;

/**
 *
 * @author apalencia
 */
public class Visita {
    
    private int id_visita;
    private String num_servicio;
    private String fecha_apertura;
    private String identificacion_tecnico;
    private String nombre_tecnico;
    private String sucursal;
    private String direccion;
    private String asunto;
    private String obs;
    private String foto;
    private String estado;
    private String cedula_solicitante;
    
    
    // respuesta  a visita tecnica
    
    private Date fecha_cierre; 
    private String obs_cierre;
    private String pendiente;
    private int inventario;
    private String imei;
    private String simcard;
    private int operador;

    public Date getFecha_cierre() {
        return fecha_cierre;
    }

    public void setFecha_cierre(Date fecha_cierre) {
        this.fecha_cierre = fecha_cierre;
    }

    public String getObs_cierre() {
        return obs_cierre;
    }

    public void setObs_cierre(String obs_cierre) {
        this.obs_cierre = obs_cierre;
    }

    public String getPendiente() {
        return pendiente;
    }

    public void setPendiente(String pendiente) {
        this.pendiente = pendiente;
    }

    public int getInventario() {
        return inventario;
    }

    public void setInventario(int inventario) {
        this.inventario = inventario;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getSimcard() {
        return simcard;
    }

    public void setSimcard(String simcard) {
        this.simcard = simcard;
    }

    public int getOperador() {
        return operador;
    }

    public void setOperador(int operador) {
        this.operador = operador;
    }
    
    
    

    public int getId_visita() {
        return id_visita;
    }

    public void setId_visita(int id_visita) {
        this.id_visita = id_visita;
    }

    public String getCedula_solicitante() {
        return cedula_solicitante;
    }

    public void setCedula_solicitante(String cedula_solicitante) {
        this.cedula_solicitante = cedula_solicitante;
    }
    
    

    public String getNum_servicio() {
        return num_servicio;
    }

    public void setNum_servicio(String num_servicio) {
        this.num_servicio = num_servicio;
    }

    public String getFecha_apertura() {
        return fecha_apertura;
    }

    public void setFecha_apertura(String fecha_apertura) {
        this.fecha_apertura = fecha_apertura;
    }

    public String getIdentificacion_tecnico() {
        return identificacion_tecnico;
    }

    public void setIdentificacion_tecnico(String identificacion_tecnico) {
        this.identificacion_tecnico = identificacion_tecnico;
    }

    public String getNombre_tecnico() {
        return nombre_tecnico;
    }

    public void setNombre_tecnico(String nombre_tecnico) {
        this.nombre_tecnico = nombre_tecnico;
    }

    public String getSucursal() {
        return sucursal;
    }

    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getAsunto() {
        return asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public String getObs() {
        return obs;
    }

    public void setObs(String obs) {
        this.obs = obs;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
    
    

    public Visita(String num_servicio, String fecha_apertura, String sucursal, String direccion, String obs, String identificacion_tecnico, String nombre_tecnico, String asunto ,String foto, String estado, String cedula_solicitante, Date fecha_cierre, String obs_cierre ,String pendiente, int inventario,String imei,String sim, int operador) {
        this.num_servicio = num_servicio;
        this.fecha_apertura = fecha_apertura;
        this.sucursal = sucursal;
        this.direccion = direccion;
        this.obs = obs;
        this.identificacion_tecnico = identificacion_tecnico;
        this.nombre_tecnico = nombre_tecnico;
        this.asunto = asunto;
        this.foto = foto;
        this.estado = estado;
        this.cedula_solicitante = cedula_solicitante;
        
        this.fecha_cierre = fecha_cierre;
        this.obs_cierre = obs_cierre;
        this.pendiente = pendiente;
        this.inventario = inventario;
        this.imei = imei;
        this.simcard = sim;
        this.operador = operador;
        
        
    }
    
    
    
}
