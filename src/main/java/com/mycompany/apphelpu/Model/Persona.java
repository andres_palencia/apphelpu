/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.apphelpu.Model;

/**
 *
 * @author apalencia
 */
public class Persona {
    
    private String nombres;
    private String pellidos;
    private String cedula;
    private String genero;
    private int    edad;

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getPellidos() {
        return pellidos;
    }

    public void setPellidos(String pellidos) {
        this.pellidos = pellidos;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public Persona(String nombres, String pellidos, String cedula, String genero, int edad) {
        this.nombres = nombres;
        this.pellidos = pellidos;
        this.cedula = cedula;
        this.genero = genero;
        this.edad = edad;
    }
    
    
    
}
