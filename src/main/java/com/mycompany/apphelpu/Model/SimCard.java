/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.apphelpu.Model;

import java.sql.Date;

/**
 *
 * @author apalencia
 */
public class SimCard {
    
    private String usuario;
    private String serial;
    private String clave;  
    private String apn; 
    private int operador; 
    private String capacidad;
    
    private String estado;
    private java.sql.Date fecha_registro;
    private int usuariosistema;
    private java.sql.Date fechaactualizacion;

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getApn() {
        return apn;
    }

    public void setApn(String apn) {
        this.apn = apn;
    }

    public int getOperador() {
        return operador;
    }

    public void setOperador(int operador) {
        this.operador = operador;
    }

    public String getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(String capacidad) {
        this.capacidad = capacidad;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Date getFecha_registro() {
        return fecha_registro;
    }

    public void setFecha_registro(Date fecha_registro) {
        this.fecha_registro = fecha_registro;
    }

    public int getUsuariosistema() {
        return usuariosistema;
    }

    public void setUsuariosistema(int usuariosistema) {
        this.usuariosistema = usuariosistema;
    }

    public Date getFechaactualizacion() {
        return fechaactualizacion;
    }

    public void setFechaactualizacion(Date fechaactualizacion) {
        this.fechaactualizacion = fechaactualizacion;
    }

    public SimCard(String usuario, String serial, String clave, String apn, int operador, String capacidad, String estado, Date fecha_registro, int usuariosistema, Date fechaactualizacion) {
        this.usuario = usuario;
        this.serial = serial;
        this.clave = clave;
        this.apn = apn;
        this.operador = operador;
        this.capacidad = capacidad;
        this.estado = estado;
        this.fecha_registro = fecha_registro;
        this.usuariosistema = usuariosistema;
        this.fechaactualizacion = fechaactualizacion;
    }
    
    
    
    
    
}
