/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.apphelpu.DAO;

import com.mycompany.apphelpu.Data.Database;
import com.mycompany.apphelpu.Model.Servicio;
import com.mycompany.apphelpu.Model.Visita;
import com.mycompany.apphelpu.Util.Resultado;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author apalencia
 */
public class TesoreriaDAO {
    
        Database conexion = null;
    PreparedStatement pst = null;
    ResultSet         rs  = null;
    
    Resultado resultado = null;
    
    public List listTopes(String usuario) {
        
        List<Servicio> listar_servicio = new LinkedList<>();
               
        try {
            try {
                conexion = new Database();
                conexion.conectar("apalencia", "asd.123*-");
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            
            pst = conexion.getCon().prepareStatement(
                    " SELECT s.num_solicitud,\n" +
"	       s.tipo_solicitud,\n" +
"		   identificacion, \n" +
"		   substring(s.direccion from 0 for 21) as direccion,\n" +
"		   sucursal,\n" +
"		   s.tipo_solicitud,\n" +
"		   s.fk_tipo_recepcion ,\n" +
"		   tp.descripcion as asunto, \n" +
"		   punto_fijo_movil,\n" +
"		   descripcion,\n" +
"		   fecha_servicio,\n" +
"		   tec.nombre || ' ' || tec.apellido as nombrecompleto,"
                            + "asg.estado\n" +
"		   \n" +
"		   \n" +
"				FROM helpdesk_core.gnr_solicitud s,\n" +
"				     helpdesk_core.gnr_tipo_asunto tp , \n" +
"					 helpdesk_core.gnr_asignacion_solicitud asg, \n" +
"					 helpdesk_core.grn_personal_tecnico tec\n" +
"					 \n" +
"					WHERE fk_asunto = id_tipo_asunto and s.tipo_solicitud = 2 and s.fk_usuario = ? \n" +
"					and asg.fk_solicitud = id_solicitud \n" +
"					and tec.id_personal_tecnico = asg.fk_tecnico \n" +
"					and s.fecha_servicio > current_date - interval '20' day\n" +
"					ORDER BY s.id_solicitud DESC	");
            pst.setInt(1, Integer.parseInt(usuario));
            rs = pst.executeQuery();
            
                
            while ( rs.next() ) {
                
                listar_servicio.add(new Servicio(rs.getString("num_solicitud"), rs.getInt("tipo_solicitud"), rs.getString("identificacion"), rs.getString("direccion"), rs.getString("sucursal"), rs.getInt("tipo_solicitud"), rs.getInt("fk_tipo_recepcion"), rs.getString("asunto"), rs.getString("punto_fijo_movil"), rs.getString("descripcion"), rs.getDate("fecha_servicio"), rs.getString("nombrecompleto"), 0, rs.getString("estado")) );
                
            }
            
            conexion.getCon().close();
            return listar_servicio;
            
        } catch (SQLException ex) {
            try {
                conexion.getCon().close();
            } catch (SQLException ex1) {
                 ex1.printStackTrace();
            }
             ex.printStackTrace();
        }
            
        try {
            conexion.getCon().close();
        } catch (SQLException ex) {
             ex.printStackTrace();
        }
              return null;  
        
        
    }
    
    
    public List historialTopes() {
        
        List<Servicio> listar_servicio = new LinkedList<>();
               
        try {
            try {
                conexion = new Database();
                conexion.conectar("apalencia", "asd.123*-");
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            
            pst = conexion.getCon().prepareStatement(
                    " SELECT s.num_solicitud,\n" +
"	       s.tipo_solicitud,\n" +
"\n" +
"		   substring(s.direccion from 0 for 21) as direccion,\n" +
"		   sucursal,\n" +
"		   s.tipo_solicitud,\n" +
"		   s.fk_tipo_recepcion ,\n" +
"		   tp.descripcion as asunto, \n" +
"		   punto_fijo_movil,\n" +
"		   descripcion,\n" +
"		   fecha_servicio,\n" +
"		   s.estado\n" +
"\n" +
"\n" +
"				FROM helpdesk_core.gnr_solicitud s,\n" +
"				     helpdesk_core.gnr_tipo_asunto tp \n" +
"		\n" +
"					\n" +
"					WHERE fk_asunto = id_tipo_asunto and s.tipo_solicitud = 2 \n" +
"\n" +
"		\n" +
"					and s.fecha_servicio > current_date - interval '20' day\n" +
"					ORDER BY s.id_solicitud DESC	");
            //pst.setInt(1, Integer.parseInt(usuario));
            rs = pst.executeQuery();
            
                
            while ( rs.next() ) {
                
                listar_servicio.add(new Servicio(rs.getString("num_solicitud"), rs.getInt("tipo_solicitud"), "", rs.getString("direccion"), rs.getString("sucursal"), rs.getInt("tipo_solicitud"), rs.getInt("fk_tipo_recepcion"), rs.getString("asunto"), rs.getString("punto_fijo_movil"), rs.getString("descripcion"), rs.getDate("fecha_servicio"), "", 0, rs.getString("estado")) );
                
            }
            
            conexion.getCon().close();
            return listar_servicio;
            
        } catch (SQLException ex) {
            try {
                conexion.getCon().close();
            } catch (SQLException ex1) {
                 ex1.printStackTrace();
            }
             ex.printStackTrace();
        }
            
        try {
            conexion.getCon().close();
        } catch (SQLException ex) {
             ex.printStackTrace();
        }
              return null;  
        
        
    }
    
    public List listaTopesByCriteria(int oper, String dato) {
        
        List listar_servicio = new LinkedList<>();
        
        String query = "";
        
        try {
            try {
                conexion = new Database();
                conexion.conectar("apalencia", "asd.123*-");
            } catch (IOException ex) {
                 ex.printStackTrace();
            }
            
            if ( dato.equals("T") ) {
                oper = 8;
            }
            
            if ( dato.equals("CA") ){
                oper = 4;
            }else if (dato.equals("AA")) {
                oper = 5;
            }
            
            if ( dato.equals("A")  ){
                   oper = 2;
            }else{
                if ( dato.equals("C") ){
                    oper = 1;
                }else{
                    if ( dato.equals("F") ){
                        oper = 3;
                    }
                }
            }
             if( dato.equals("P") ) {
                        oper = 6;
                    }
            
            
            switch(oper) {
                
                case 1:
                    
                     query = "SELECT *, \n" +
"       tp.descripcion as asunto\n" +
"\n" +
"	   FROM \n" +
"	   helpdesk_core.gnr_solicitud sol, \n" +
"	   helpdesk_core.gnr_tipo_asunto tp \n" +
"	   \n" +
"	   WHERE sol.fk_asunto = tp.id_tipo_asunto \n" +
"	   and sol.estado =?\n" +
"          ";
                        pst = conexion.getCon().prepareStatement(query);
                        pst.setString(1, dato);
                        
                    break;
                    
                case 2:
                    
                    query = "SELECT sol.*, \n" +
"       tp.descripcion as asunto, \n" +
"	   tec.nombre || ' ' || tec.apellido as nombrecompleto, \n" +
"	   tiposerv.descripcion as desc_tipo_serv \n" +
"	  \n" +
"	   FROM helpdesk_core.gnr_solicitud sol,\n" +
"	        helpdesk_core.gnr_tipo_asunto tp ,\n" +
"			helpdesk_core.gnr_asignacion_solicitud asg, \n" +
"			helpdesk_core.grn_personal_tecnico tec,\n" +
"			helpdesk_core.gnr_tipo_servicio tiposerv \n" +
"			\n" +
"			WHERE fk_asunto = id_tipo_asunto \n" +
"			and asg.fk_solicitud = sol.id_solicitud \n" +
"			and tec.id_personal_tecnico = asg.fk_tecnico\n" +
"		        and asg.fk_tipo_servicio = tiposerv.id_tipo_servicio \n" +
"  			and sol.estado = 'A'\n" +
"			and asg.estado != 'F'"; 
                    pst = conexion.getCon().prepareStatement(query);
                    //pst.setString(1, dato);
                    
                    break;
                    
                case 3:
                    
                     query = " SELECT *, \n" +
"       tp.descripcion as asunto, \n" +
"	   tec.nombre || ' ' || tec.apellido as nombrecompleto, \n" +
"	   tiposerv.descripcion as desc_tipo_serv \n" +
"	   \n" +
"	   FROM helpdesk_core.gnr_solicitud, \n" +
"	        helpdesk_core.gnr_tipo_asunto tp ,\n" +
"			helpdesk_core.gnr_asignacion_solicitud asg, \n" +
"			helpdesk_core.grn_personal_tecnico tec,\n" +
"			helpdesk_core.gnr_tipo_servicio tiposerv \n" +
"			\n" +
"			WHERE fk_asunto = id_tipo_asunto \n" +
"			and asg.fk_solicitud = id_solicitud \n" +
"			and tec.id_personal_tecnico = asg.fk_tecnico\n" +
"			and asg.fk_tipo_servicio = tiposerv.id_tipo_servicio \n" +
"			and asg.estado = 'F'"
                            + ""; 
                     pst = conexion.getCon().prepareStatement(query);
                     //pst.setString(1, dato);
                    
                    break;
                    
                    
                case 4 :
                        
                     query = "SELECT *, \n" +
                                "       tp.descripcion as asunto\n" +
                                "\n" +
                                "	   FROM \n" +
                                "	   helpdesk_core.gnr_solicitud sol, \n" +
                                "	   helpdesk_core.gnr_tipo_asunto tp \n" +
                                "	   \n" +
                                "	   WHERE sol.fk_asunto = tp.id_tipo_asunto \n" +
                                "	   and sol.estado ='C'\n" +
                                "          and sol.fecha_servicio::DATE = NOW()::DATE";
                        pst = conexion.getCon().prepareStatement(query);
                        //pst.setString(1, dato);
                    
                    break;
                    
                case 5 :
                    
                query = " SELECT *, \n" +
"       tp.descripcion as asunto, \n" +
"	   tec.nombre || ' ' || tec.apellido as nombrecompleto, \n" +
"	   tiposerv.descripcion as desc_tipo_serv \n" +
"	   \n" +
"	   FROM helpdesk_core.gnr_solicitud, \n" +
"	        helpdesk_core.gnr_tipo_asunto tp ,\n" +
"			helpdesk_core.gnr_asignacion_solicitud asg, \n" +
"			helpdesk_core.grn_personal_tecnico tec,\n" +
"			helpdesk_core.gnr_tipo_servicio tiposerv \n" +
"			\n" +
"			WHERE fk_asunto = id_tipo_asunto \n" +
"			and asg.fk_solicitud = id_solicitud \n" +
"			and tec.id_personal_tecnico = asg.fk_tecnico\n" +
"			and asg.fk_tipo_servicio = tiposerv.id_tipo_servicio \n" +
"			and asg.estado = 'A'"
                            + "and helpdesk_core.gnr_solicitud.fecha_servicio::DATE = NOW()::DATE"; 
                    pst = conexion.getCon().prepareStatement(query);
                    //pst.setString(1, dato);
                    
                    break;
                    
                case 6 :
                    
                   query = " SELECT *, \n" +
"       tp.descripcion as asunto, \n" +
"	   tec.nombre || ' ' || tec.apellido as nombrecompleto, \n" +
"	   tiposerv.descripcion as desc_tipo_serv \n" +
"	   \n" +
"	   FROM helpdesk_core.gnr_solicitud, \n" +
"	        helpdesk_core.gnr_tipo_asunto tp ,\n" +
"			helpdesk_core.gnr_asignacion_solicitud asg, \n" +
"			helpdesk_core.grn_personal_tecnico tec,\n" +
"			helpdesk_core.gnr_tipo_servicio tiposerv \n" +
"			\n" +
"			WHERE fk_asunto = id_tipo_asunto \n" +
"			and asg.fk_solicitud = id_solicitud \n" +
"			and tec.id_personal_tecnico = asg.fk_tecnico\n" +
"			and asg.fk_tipo_servicio = tiposerv.id_tipo_servicio \n" +
"			and asg.estado = ?"
                            + ""; 
                    pst = conexion.getCon().prepareStatement(query);
                    pst.setString(1, dato);
                    
                    break;
                    
                case 8 :
                    
                    
                    
                    break;
                    
                    
                default:
                    
                    
                     break;
                    
                
            }
                       
            rs = pst.executeQuery();
            
            while ( rs.next() ) {
                
              listar_servicio.add(new Servicio(rs.getInt("id_solicitud"),rs.getString("num_solicitud"), rs.getInt("tipo_solicitud"), rs.getString("documento"), rs.getString("direccion"), rs.getString("sucursal"), rs.getInt("tipo_solicitud"), rs.getInt("fk_tipo_recepcion"), rs.getString("asunto"), rs.getString("punto_fijo_movil"), "", rs.getDate("fecha_servicio"), "", 0,"") );
                
            }
            
            conexion.getCon().close();
            return listar_servicio;
            
        } catch (SQLException ex) {
            try {
                conexion.getCon().close();
            } catch (SQLException ex1) {
                 ex.printStackTrace();
            }
             ex.printStackTrace();
        }
            
        try {
            conexion.getCon().close();
        } catch (SQLException ex) {
             ex.printStackTrace();
        }
              return null;  
        
        
    }
    
    
    public List detalleHistorialTope(Object id) {
        
        List listar_servicio = new LinkedList<>();
        
        try {
            try {
                conexion = new Database();
                conexion.conectar("apalencia", "asd.123*-");
            } catch (IOException ex) {
                 ex.printStackTrace();
            }
            
             pst = conexion.getCon().prepareStatement("SELECT\n" +
"       sol.documento,\n" +
"       sol.num_solicitud, \n" +
"       sol.punto_fijo_movil,\n" +
"	   sol.obs,\n" +
"	   sol.direccion,\n" +
"       sol.estado,sol.sucursal,\n" +
"	   sol.fecha_servicio,\n" +
"	   tp.id_tipo_asunto,\n" +
"	   tp.descripcion as asunto \n" +
"	   \n" +
"FROM helpdesk_core.gnr_solicitud sol,\n" +
"     helpdesk_core.gnr_tipo_asunto tp\n" +
"\n" +
"WHERE sol.tipo_solicitud = 2 and sol.num_solicitud = ?\n" +
"\n" +
"AND tp.id_tipo_asunto = sol.fk_asunto ;");
            
            pst.setString(1, id.toString());
            rs = pst.executeQuery();
            
            while ( rs.next() ) {
                
             listar_servicio.add(rs.getString(1));
                listar_servicio.add(rs.getString(2));
                listar_servicio.add(rs.getString(3));
                listar_servicio.add(rs.getString(4));
                listar_servicio.add(rs.getString(5));
                listar_servicio.add(rs.getString(6));
                listar_servicio.add(rs.getString(7));
                listar_servicio.add(rs.getDate(8));
                listar_servicio.add(rs.getInt(9));
                listar_servicio.add(rs.getString(10));
                
                //System.out.println(""+listar_servicio);
                
            }
            
            //System.out.println(""+listar_servicio);
            
            conexion.getCon().close();
            return listar_servicio;
            
        } catch (SQLException ex) {
            try {
                conexion.getCon().close();
                 ex.printStackTrace();
            } catch (SQLException ex1) {
                 ex1.printStackTrace();
            }
        }
            
        try {
            conexion.getCon().close();
        } catch (SQLException ex) {
             ex.printStackTrace();
        }
              return null;  
        
        
    }
    
    public Visita cargarDatosDeVisita(String os) throws SQLException{
        
        Visita visita = null;
        
        try {
                conexion = new Database();
                conexion.conectar("apalencia", "asd.123*-");
            } catch (IOException ex) {
                 ex.printStackTrace();
            }
            
            pst = conexion.getCon().prepareStatement("select * from helpdesk_core.historial_visita(?)");
            pst.setString(1, os);
           
 
            
            rs = pst.executeQuery();
            
            while ( rs.next() ) {
                    
                visita = new Visita(rs.getString("num_solicitud"), rs.getString("fecha_servicio"), rs.getString("sucursal"), rs.getString("direccion"), rs.getString("obs"), rs.getString("identificacion"), rs.getString("nombre_tecnico"), rs.getString("descripcion"),rs.getString("foto_tec"), rs.getString("estado"), rs.getString("documento"),rs.getDate("fecha_cierre"), rs.getString("obs_cierre"), rs.getString("pendiente"), rs.getInt("inventario"), rs.getString("imei"), rs.getString("sim"), rs.getInt("operador") );
                
            }
            
            rs.close();
            pst.close();
            this.conexion.getCon().close();
            return visita;

        
    }
    
    public Resultado genera_controlAcceso(String idservicio,String qr) throws SQLException{
        
         try {
                conexion = new Database();
                conexion.conectar("apalencia", "asd.123*-");
            } catch (IOException ex) {
                 ex.printStackTrace();
            }
            
            pst = conexion.getCon().prepareStatement("select * from helpdesk_core.validar_vista(?,?)");
            pst.setString(1, idservicio);
            pst.setString(2, qr);
 
            
            rs = pst.executeQuery();
            
            while ( rs.next() ) {
                    
                resultado = new Resultado(rs.getInt(1), rs.getString(2), rs.getString(3));
                
            }
                        this.conexion.getCon().close();
            return resultado;

        
        
        
    }
    
    //// TESORE_RIA
    
    public List listServiciosByCriteriaSeguridad(int oper, String dato) {
        
        List listar_servicio = new LinkedList<>();
        
        String query = "";
        
        try {
            try {
                conexion = new Database();
                conexion.conectar("apalencia", "asd.123*-");
            } catch (IOException ex) {
                 ex.printStackTrace();
            }
            
            if ( dato.equals("CA") ){
                oper = 4;
            }else if (dato.equals("AA")) {
                oper = 5;
            }
            
            if ( dato.equals("A")  ){
                   oper = 2;
            }else{
                if ( dato.equals("C") ){
                    oper = 1;
                }else{
                    if ( dato.equals("F") ){
                        oper = 3;
                    }
                }
            }
             if( dato.equals("P") || dato.equals("SF") ) {
                        oper = 6;
                    }
            
            
            switch(oper) {
                
                case 1:
                    
                     query = "SELECT *, \n" +
"       tp.descripcion as asunto\n" +
"\n" +
"	   FROM \n" +
"	   helpdesk_core.gnr_solicitud sol, \n" +
"	   helpdesk_core.gnr_tipo_asunto tp \n" +
"	   \n" +
"	   WHERE sol.fk_asunto = tp.id_tipo_asunto and sol.tipo_solicitud = 2 \n" +
"	   and sol.estado =? ORDER BY id_solicitud DESC \n" +
"          ";
                        pst = conexion.getCon().prepareStatement(query);
                        pst.setString(1, dato);
                        
                    break;
                    
                case 2:
                    
                    query = "SELECT sol.*, \n" +
"       tp.descripcion as asunto, \n" +
"	   tec.nombre || ' ' || tec.apellido as nombrecompleto, \n" +
"	   tiposerv.descripcion as desc_tipo_serv \n" +
"	  \n" +
"	   FROM helpdesk_core.gnr_solicitud sol,\n" +
"	        helpdesk_core.gnr_tipo_asunto tp ,\n" +
"			helpdesk_core.gnr_asignacion_solicitud asg, \n" +
"			helpdesk_core.grn_personal_tecnico tec,\n" +
"			helpdesk_core.gnr_tipo_servicio tiposerv \n" +
"			\n" +
"			WHERE fk_asunto = id_tipo_asunto \n" +
"			and asg.fk_solicitud = sol.id_solicitud \n" +
"			and tec.id_personal_tecnico = asg.fk_tecnico\n" +
"		        and asg.fk_tipo_servicio = tiposerv.id_tipo_servicio \n" +
"  			and sol.estado = 'A' and sol.tipo_solicitud = 2 \n" +
"			and asg.estado != 'F' ORDER BY id_solicitud DESC "; 
                    pst = conexion.getCon().prepareStatement(query);
                    //pst.setString(1, dato);
                    
                    break;
                    
                case 3:
                    
                     query = "SELECT sol.id_solicitud,\n" +
"	   sol.num_solicitud,\n" +
"	   sol.fk_solicitante,\n" +
"	   sol.documento,\n" +
"	   sol.direccion,\n" +
"	   sol.sucursal,\n" +
"	   sol.tipo_solicitud,\n" +
"	   sol.fk_tipo_recepcion,\n" +
"	   sol.fk_asunto,\n" +
"	   sol.punto_fijo_movil,\n" +
"	   tiposerv.descripcion as desc_tipo_serv,\n" +
"	   sol.fecha_servicio,\n" +
"	   sol.imagen,\n" +
"	   asg.fk_usuario,\n" +
"	   asg.estado,\n" +
"	   sol.usuario_actualizacion,\n" +
"          tp.descripcion as asunto,\n" +
"	   sol.id_punto_venta\n" +
"	   --tec.nombre || ' ' || tec.apellido as nombrecompleto\n" +
"	   \n" +
"	   \n" +
"	   FROM helpdesk_core.gnr_solicitud sol, \n" +
"	        helpdesk_core.gnr_tipo_asunto tp ,\n" +
"			helpdesk_core.gnr_asignacion_solicitud asg, \n" +
"			helpdesk_core.grn_personal_tecnico tec,\n" +
"			helpdesk_core.gnr_tipo_servicio tiposerv \n" +
"			\n" +
"			WHERE sol.fk_asunto = id_tipo_asunto \n" +
"			and asg.fk_solicitud = id_solicitud \n" +
"			and tec.id_personal_tecnico = asg.fk_tecnico\n" +
"			and asg.fk_tipo_servicio = tiposerv.id_tipo_servicio \n" +
"			and asg.estado = 'F' and tipo_solicitud = 2 ORDER BY id_solicitud DESC "
                            + ""; 
                     pst = conexion.getCon().prepareStatement(query);
                     //pst.setString(1, dato);
                    
                    break;
                    
                    
                case 4 :
                        
                     query = "SELECT *, \n" +
                                "       tp.descripcion as asunto\n" +
                                "\n" +
                                "	   FROM \n" +
                                "	   helpdesk_core.gnr_solicitud sol, \n" +
                                "	   helpdesk_core.gnr_tipo_asunto tp \n" +
                                "	   \n" +
                                "	   WHERE sol.fk_asunto = tp.id_tipo_asunto \n" +
                                "	   and sol.estado ='C' and sol.tipo_solicitud = 2\n" +
                                "          and sol.fecha_servicio::DATE = NOW()::DATE ORDER BY id_solicitud DESC ";
                        pst = conexion.getCon().prepareStatement(query);
                        //pst.setString(1, dato);
                    
                    break;
                    
                case 5 :
                    
                query = " SELECT *, \n" +
"       tp.descripcion as asunto, \n" +
"	   tec.nombre || ' ' || tec.apellido as nombrecompleto, \n" +
"	   tiposerv.descripcion as desc_tipo_serv \n" +
"	   \n" +
"	   FROM helpdesk_core.gnr_solicitud, \n" +
"	        helpdesk_core.gnr_tipo_asunto tp ,\n" +
"			helpdesk_core.gnr_asignacion_solicitud asg, \n" +
"			helpdesk_core.grn_personal_tecnico tec,\n" +
"			helpdesk_core.gnr_tipo_servicio tiposerv \n" +
"			\n" +
"			WHERE fk_asunto = id_tipo_asunto \n" +
"			and asg.fk_solicitud = id_solicitud \n" +
"			and tec.id_personal_tecnico = asg.fk_tecnico\n" +
"			and asg.fk_tipo_servicio = tiposerv.id_tipo_servicio \n" +
"			and asg.estado = 'A' and tipo_solicitud = 2 "
                            + "and helpdesk_core.gnr_solicitud.fecha_servicio::DATE = NOW()::DATE ORDER BY id_solicitud DESC "; 
                    pst = conexion.getCon().prepareStatement(query);
                    //pst.setString(1, dato);
                    
                    break;
                    
                case 6 :
                    
                   query = "SELECT sol.id_solicitud,\n" +
"	   sol.num_solicitud,\n" +
"	   sol.fk_solicitante,\n" +
"	   sol.documento,\n" +
"	   sol.direccion,\n" +
"	   sol.sucursal,\n" +
"	   sol.tipo_solicitud,\n" +
"	   sol.fk_tipo_recepcion,\n" +
"	   sol.fk_asunto,\n" +
"	   sol.punto_fijo_movil,\n" +
"	   tiposerv.descripcion as desc_tipo_serv,\n" +
"	   sol.fecha_servicio,\n" +
"	   sol.imagen,\n" +
"	   asg.fk_usuario,\n" +
"   	   asg.estado,\n" +
"	   sol.usuario_actualizacion,\n" +
"          tp.descripcion as asunto,\n" +
"	   sol.id_punto_venta\n" +
"	   \n" +
"	   FROM helpdesk_core.gnr_solicitud sol, \n" +
"	        helpdesk_core.gnr_tipo_asunto tp,\n" +
"		helpdesk_core.gnr_asignacion_solicitud asg, \n" +
"		helpdesk_core.grn_personal_tecnico tec,\n" +
"		helpdesk_core.gnr_tipo_servicio tiposerv \n" +
"			\n" +
"			WHERE sol.fk_asunto =  tp.id_tipo_asunto \n" +
"			and asg.fk_solicitud = sol.id_solicitud \n" +
"			and tec.id_personal_tecnico = asg.fk_tecnico\n" +
"			and asg.fk_tipo_servicio = tiposerv.id_tipo_servicio\n" +
"                       and sol.tipo_solicitud = 2\n" +
"			and asg.estado = ?\n" +
"			ORDER BY sol.id_solicitud DESC"; 
                    pst = conexion.getCon().prepareStatement(query);
                    pst.setString(1, dato);
                    
                    break;
                    
                    
                default:
                    
                    
                     break;
                    
                
            }
            
            
            rs = pst.executeQuery();
            
            while ( rs.next() ) {
                
              listar_servicio.add(new Servicio(rs.getInt("id_solicitud"),rs.getString("num_solicitud"), rs.getInt("tipo_solicitud"), rs.getString("documento"), rs.getString("direccion"), rs.getString("sucursal"), rs.getInt("tipo_solicitud"), rs.getInt("fk_tipo_recepcion"), rs.getString("asunto"), rs.getString("punto_fijo_movil"), "", rs.getDate("fecha_servicio"), "", 0,rs.getString("estado")) );
                
            }
            
            conexion.getCon().close();
            return listar_servicio;
            
        } catch (SQLException ex) {
            try {
                conexion.getCon().close();
            } catch (SQLException ex1) {
                 ex.printStackTrace();
            }
             ex.printStackTrace();
        }
            
        try {
            conexion.getCon().close();
        } catch (SQLException ex) {
             ex.printStackTrace();
        }
              return null;  
        
        
    }
    
    public Resultado cerrarServicio(String servicio, int usuario, String descripcion, String estado, String pendiente,int inventario, String imei, String simcard, int operador, String cierre) throws SQLException{
        
          try {
                conexion = new Database();
                conexion.conectar("apalencia", "asd.123*-");
            } catch (IOException ex) {
                 ex.printStackTrace();
            }
            
            pst = conexion.getCon().prepareStatement("select * from helpdesk_core.cerrar_servicio(?,?,?,?,?,?,?,?,?,?)");
            pst.setString(1, servicio);
            pst.setInt(2, usuario);
            pst.setString(3, descripcion);
            pst.setString(4, estado);
            pst.setString(5, pendiente);
            pst.setInt(6, inventario);
            pst.setString(7, imei);
            pst.setString(8, simcard);
            pst.setInt(9, operador);
            pst.setString(10, cierre);
            
            rs = pst.executeQuery();
            
            while ( rs.next() ) {
                    
                resultado = new Resultado(rs.getInt(1), rs.getString(2), rs.getString(3));
                
            }
                        this.conexion.getCon().close();
            return resultado;

            
        
    }
    
    
    ////// DETALLE VISITA ACTUAL VISITA mis SOLICITUDES
    
    public Visita cargarDatosMisSolicitudesDeVisita(String os) throws SQLException{
        
        Visita visita = null;
        
        try {
                conexion = new Database();
                conexion.conectar("apalencia", "asd.123*-");
            } catch (IOException ex) {
                 ex.printStackTrace();
            }
            
            pst = conexion.getCon().prepareStatement("select * from helpdesk_core.info_visita(?)");
            pst.setString(1, os);
           
 
            
            rs = pst.executeQuery();
            
            while ( rs.next() ) {
                    
                visita = new Visita(rs.getString("num_solicitud"), rs.getString("fecha_servicio"), rs.getString("sucursal"), rs.getString("direccion"), rs.getString("obs"), rs.getString("identificacion"), rs.getString("nombre_tecnico"), rs.getString("descripcion"),rs.getString("foto_tec"), rs.getString("estado"), rs.getString("documento"),rs.getDate("fecha_cierre"), rs.getString("obs_cierre"), rs.getString("pendiente"), rs.getInt("inventario"), rs.getString("imei"), rs.getString("sim"), rs.getInt("operador") );
                
            }
            
            rs.close();
            pst.close();
            this.conexion.getCon().close();
            return visita;

        
    }
    
    public java.util.List<Servicio> listarServiciosPorFechaTeso(java.sql.Date fechai, java.sql.Date fechaf){
        
        List<Servicio> listar_servicio = new LinkedList<>();
        
        try {
            try {
                conexion = new Database();
                conexion.conectar("apalencia", "asd.123*-");
            } catch (IOException ex) {
                
            }

                pst = conexion.getCon().prepareStatement("SELECT     s.num_solicitud,\n" +
"	       s.tipo_solicitud,\n" +
"		   identificacion, \n" +
"		   substring(s.direccion from 0 for 21) as direccion,\n" +
"		   sucursal,\n" +
"		   s.tipo_solicitud,\n" +
"		   s.fk_tipo_recepcion ,\n" +
"		   tp.descripcion as asunto, \n" +
"		   punto_fijo_movil,\n" +
"		   descripcion,\n" +
"		   fecha_servicio,\n" +
"		   tec.nombre || ' ' || tec.apellido as nombrecompleto,\n" +
"           asg.estado\n" +
"		   \n" +
"		   \n" +
"				FROM helpdesk_core.gnr_solicitud s,\n" +
"				     helpdesk_core.gnr_tipo_asunto tp , \n" +
"					 helpdesk_core.gnr_asignacion_solicitud asg, \n" +
"					 helpdesk_core.grn_personal_tecnico tec\n" +
"					 \n" +
"					WHERE fk_asunto = id_tipo_asunto \n" +
"					and asg.fk_solicitud = id_solicitud \n" +
"					and tec.id_personal_tecnico = asg.fk_tecnico and s.tipo_solicitud = 2 \n" +
"					and s.fecha_servicio::date between ?::date and ?::date\n" +
"					ORDER BY s.id_solicitud DESC");
                
                pst.setDate(1, fechai);
                pst.setDate(2, fechaf);
            
            rs = pst.executeQuery();
            
            while ( rs.next() ) {
                
                listar_servicio.add(new Servicio(rs.getString("num_solicitud"), rs.getInt("tipo_solicitud"), rs.getString("identificacion"), rs.getString("direccion"), rs.getString("sucursal"), rs.getInt("tipo_solicitud"), rs.getInt("fk_tipo_recepcion"), rs.getString("asunto"), rs.getString("punto_fijo_movil"), rs.getString("descripcion"), rs.getDate("fecha_servicio"), rs.getString("nombrecompleto"), 0, rs.getString("estado")) );
                
            }
            
            conexion.getCon().close();
            return listar_servicio;
            
        } catch (SQLException ex) {
            try {
                conexion.getCon().close();
            } catch (SQLException ex1) {
                 ex.printStackTrace();
            }
             ex.printStackTrace();
        }
            
        try {
            conexion.getCon().close();
        } catch (SQLException ex) {
             ex.printStackTrace();
        }
              return null;  
        
        
    }
    
    
}
