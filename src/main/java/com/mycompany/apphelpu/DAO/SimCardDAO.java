/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.apphelpu.DAO;

import com.mycompany.apphelpu.Data.Database;
import com.mycompany.apphelpu.Facade.IDSimCard;
import com.mycompany.apphelpu.Model.AsignarServicio;
import com.mycompany.apphelpu.Model.AsignarSim;
import com.mycompany.apphelpu.Model.Servicio;
import com.mycompany.apphelpu.Model.SimCard;
import com.mycompany.apphelpu.Util.Resultado;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author apalencia
 */
public class SimCardDAO implements IDSimCard<SimCard, Long>{
    
    Database conexion = null;
    PreparedStatement pst = null;
    ResultSet         rs  = null;
    
    Resultado resultado = null;

    @Override
    public Resultado addSim(SimCard sc) {
        
        try {
            
            conexion = new Database();
            conexion.conectar("apalencia", "asd.123*-");
                        
            pst = conexion.getCon().prepareStatement("select * from simcard_core.registrosim(?,?,?,?,?,?,?,?)");
            
            pst.setString(1, sc.getUsuario());
            pst.setString(2, sc.getSerial());
            pst.setString(3, sc.getClave());
            pst.setString(4, sc.getApn());
            pst.setInt(5, sc.getOperador());
            pst.setString(6, sc.getCapacidad());
            pst.setString(7, sc.getEstado());
            pst.setInt(8, sc.getUsuariosistema());
            
            rs = pst.executeQuery();
            
            while ( rs.next() ) {
                    
                resultado = new Resultado(rs.getInt(1), rs.getString(2), rs.getString(3));
                
            }
            
            
            conexion.getCon().close();
            return resultado;
            
            
        } catch (SQLException ex) {
            try {
                conexion.getCon().close();
            } catch (SQLException ex1) {
                ex1.printStackTrace();
            }
            
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        
        return null;
        
        
        
    }

    @Override
    public Resultado asignarSimCard(AsignarSim asg) {
            
          try {
            
            conexion = new Database();
            conexion.conectar("apalencia", "asd.123*-");
                        
            pst = conexion.getCon().prepareStatement("select * from simcard_core.asignarsim(?,?,?,?,?,?,?)");
            
            pst.setString(1, asg.getIdentificacion());
            pst.setString(2, asg.getPto_venta());
            pst.setString(3, asg.getPuerto());
            pst.setString(4, asg.getObs());
            pst.setString(5, asg.getUsuario());
            pst.setString(6, asg.getNombre());
            pst.setInt(7, asg.getUsuariosis());
 
            
            rs = pst.executeQuery();
            
            while ( rs.next() ) {
                    
                resultado = new Resultado(rs.getInt(1), rs.getString(2), rs.getString(3));
                
            }
            
            
            conexion.getCon().close();
            return resultado;
            
            
        } catch (SQLException ex) {
            try {
                conexion.getCon().close();
            } catch (SQLException ex1) {
                ex1.printStackTrace();
            }
            
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        
        return null;
        
        
        
    }

    @Override
    public Servicio updateSimCard(SimCard serv) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<SimCard> listSimcard() {
        
        List<SimCard> list_sim = new ArrayList<>();
        
        try {
            
            conexion = new Database();
            conexion.conectar("apalencia", "asd.123*-");
                        
            pst = conexion.getCon().prepareStatement("SELECT simc_regsimcard_usuario, simc_regsimcard_serial, simc_regsimcard_clave, simc_regsimcard_apn, simc_regsimcard_operador, simc_regsimcard_capacidad, simc_regsimcard_estado, simc_regsimcard_fecha_reg, simc_regsimcard_uus, simc_regsimcard_fechup\n" +
"	FROM simcard_core.simc_regsimcard ORDER BY  simc_regsimcard_id DESC");
                   
            rs = pst.executeQuery();
            
            while ( rs.next() ) {
               list_sim.add(new SimCard(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getInt(5), rs.getString(6), rs.getString(7), rs.getDate(8), rs.getInt(9), null));
                               
            }
            
            
            conexion.getCon().close();
            return list_sim;          
            
        } catch (SQLException ex) {
            try {
                conexion.getCon().close();
            } catch (SQLException ex1) {
                ex1.printStackTrace();
            }
            
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        
        return null;
        
        
    }

  
    public SimCard obtenerSim(String sim){
        
        try {
            
            conexion = new Database();
            conexion.conectar("apalencia", "asd.123*-");
                        
            pst = conexion.getCon().prepareStatement("SELECT simc_regsimcard_usuario, simc_regsimcard_serial, simc_regsimcard_clave, simc_regsimcard_apn, simc_regsimcard_operador, simc_regsimcard_capacidad, simc_regsimcard_estado, simc_regsimcard_fecha_reg, simc_regsimcard_uus, simc_regsimcard_fechup\n" +
"	FROM simcard_core.simc_regsimcard WHERE simc_regsimcard_usuario = ? ");
                   
            pst.setString(1, sim);
            rs = pst.executeQuery();
            
            while ( rs.next() ) {
               
               return new SimCard(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getInt(5), rs.getString(6), rs.getString(7), rs.getDate(8), rs.getInt(9), null);
            }
            
            
            conexion.getCon().close();
                      
            
        } catch (SQLException ex) {
            try {
                conexion.getCon().close();
            } catch (SQLException ex1) {
                ex1.printStackTrace();
            }
            
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        
        return null;
        
    }
    
    
}
