/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.apphelpu.Facade;

import com.mycompany.apphelpu.Model.AsignarServicio;
import com.mycompany.apphelpu.Model.AsignarSim;
import com.mycompany.apphelpu.Model.Servicio;
import com.mycompany.apphelpu.Model.SimCard;
import com.mycompany.apphelpu.Util.Resultado;
import java.util.List;

/**
 *
 * @author apalencia
 * @param <T>
 * @param <E>
 */
public interface IDSimCard<T,E> {
    
    Resultado  addSim(SimCard s);
    Resultado  asignarSimCard(AsignarSim asg);
    Servicio updateSimCard(T serv);
    List listSimcard();
    
}
