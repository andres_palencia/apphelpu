/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.apphelpu.Services;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.mycompany.apphelpu.DAO.SimCardDAO;
import com.mycompany.apphelpu.Model.AsignarSim;
import com.mycompany.apphelpu.Model.Asunto;
import com.mycompany.apphelpu.Model.SimCard;
import com.mycompany.apphelpu.Util.Resultado;
import java.lang.reflect.Type;
import java.util.LinkedList;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * REST Web Service
 *
 * @author apalencia
 */
@Path("simcard")
public class SimcardResource {

    @Context
    private UriInfo context;
    
    private SimCardDAO simc = new SimCardDAO();
    Gson gson = new Gson();

    /**
     * Creates a new instance of SimcardResource
     */
    public SimcardResource() {
    }

    /**
     * Retrieves representation of an instance of com.mycompany.apphelpu.Services.SimcardResource
     * @return an instance of java.lang.String
     */
    @GET
    @Path("hola")
    @Produces(MediaType.APPLICATION_JSON)
    public String getJson() {
        //TODO return proper representation object
        return "Servicio de prueba para modulo simcard";
    }

    /**
     * PUT method for updating or creating an instance of SimcardResource
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void putJson(String content) {
    }
    
    @POST
    @Path("regsim")
    @Consumes( { MediaType.APPLICATION_FORM_URLENCODED, MediaType.APPLICATION_XML } )
    @Produces(MediaType.APPLICATION_JSON)
    
    public Response regsimcard(
    
            @FormParam("usuario") String usuario,
            @FormParam("serial")  String serial,
            @FormParam("clave")   String clave,
            @FormParam("apn")     String apn,
            @FormParam("operador") String operador,
            @FormParam("capacidad") String capacidad,
            @FormParam("estado")    String estado,
            @FormParam("usuariosistema")       String uss
                    
                    
                    
    ){
        
        
         JsonObject object = new JsonObject();
         
         Resultado res = this.simc.addSim(new SimCard(usuario, serial, clave, apn, Integer.parseInt(operador), capacidad, estado, null, Integer.parseInt(uss), null));
         object.addProperty("codigo", res.getCodigo());
         object.addProperty("resultado", res.getResultado());
         object.addProperty("estado", res.getEstado());
           
                  
           return Response.ok(object.toString(), MediaType.APPLICATION_JSON)
                          .header("Access-Control-Allow-Origin", "*")
                          .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
                          .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With")
                          
                          .build();
        
        
        
    }
    
    @GET
    @Path("consulta")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    
    public Response consultarSim(){
        
         List<SimCard> listsim =  simc.listSimcard();
        Type listType           =  new TypeToken<LinkedList<SimCard>>(){}.getType();

        String json = gson.toJson(listsim, listType);
        
       return Response.ok(json, MediaType.APPLICATION_JSON)
                          .header("Access-Control-Allow-Origin", "*")
                          .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
                          .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With")
                          
                          .build();
        
    }
    
    @GET
    @Path("validarsim/{sim}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    
    public Response validarSim(@PathParam("sim") String sim ){
    
         JsonObject object = new JsonObject();
         
         SimCard simc = this.simc.obtenerSim(sim);
         object.addProperty("usuario",    simc.getUsuario());
         object.addProperty("serial",     simc.getSerial());
         object.addProperty("clave",      simc.getClave());
         object.addProperty("apn",        simc.getApn());
         object.addProperty("operador",   simc.getOperador());
         object.addProperty("capacidad",  simc.getCapacidad());
         object.addProperty("estado",     simc.getEstado());
         object.addProperty("fecha",      simc.getFecha_registro().toString());
                  
           return Response.ok(object.toString(), MediaType.APPLICATION_JSON)
                          .header("Access-Control-Allow-Origin", "*")
                          .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
                          .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With")
                          
                          .build();
        
        
    }
    
    
    @POST
    @Path("asignarsim")
    @Consumes( { MediaType.APPLICATION_FORM_URLENCODED, MediaType.APPLICATION_XML } )
    @Produces(MediaType.APPLICATION_JSON)
    
    public Response asignarSim(
    
            @FormParam("identificacion") String identificacion,
            @FormParam("punto")  String punto,
            @FormParam("puerto")   String puerto,
            @FormParam("obs")     String obs,
            @FormParam("usuario") String usuario,
            @FormParam("nombre") String nombre,
            @FormParam("usuariosistema")       String uss
                    
                    
                    
    ){
        
        
         JsonObject object = new JsonObject();
         
         Resultado res = this.simc.asignarSimCard(new AsignarSim(identificacion, punto, puerto, obs, usuario, nombre, Integer.parseInt(uss)));
         object.addProperty("codigo", res.getCodigo());
         object.addProperty("resultado", res.getResultado());
         object.addProperty("estado", res.getEstado());
           
                  
           return Response.ok(object.toString(), MediaType.APPLICATION_JSON)
                          .header("Access-Control-Allow-Origin", "*")
                          .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
                          .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With")
                          
                          .build();
        
        
        
    }
    
    
}
