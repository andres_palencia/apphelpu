/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.apphelpu.Services;

import com.google.gson.JsonObject;
import com.mycompany.apphelpu.Model.Persona;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * REST Web Service
 *
 * @author apalencia
 */
@Path("persona")
public class PersonaResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of PersonaResource
     */
    public PersonaResource() {
    }

    /**
     * Retrieves representation of an instance of com.mycompany.apphelpu.Services.PersonaResource
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_XML)
    public String getXml() {
        //TODO return proper representation object
        throw new UnsupportedOperationException();
    }

    /**
     * PUT method for updating or creating an instance of PersonaResource
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.APPLICATION_XML)
    public void putXml(String content) {
    }
    
    @POST
    @Path("crearp")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    
    public Response crearPersona(
    @FormParam("cedula") String cedula,
    @FormParam("nombres") String nombres,
    @FormParam("apellidos") String apellidos,
    @FormParam("genero")  String genero,
    @FormParam("edad")    int edad
    
    ){
        
        Persona p = new Persona(nombres, apellidos, cedula, genero, edad);
        
        JsonObject object = new JsonObject();
           
           object.addProperty("persona", "Hola ! aqui me estas creanndo");
           object.addProperty("cedula", p.getCedula());
           object.addProperty("nombres", p.getNombres());
           object.addProperty("apellidos", p.getPellidos());
           object.addProperty("genero", p.getGenero());
           object.addProperty("edad", p.getEdad());
           
                  
           return Response.ok(object.toString(), MediaType.APPLICATION_JSON)
                          .header("Access-Control-Allow-Origin", "*")
                          .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
                          .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With")
                          
                          .build();
        
    }
    
    
    @GET 
    @Path("consultarp")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    
    public Response consultarPersona(){
        
        JsonObject object = new JsonObject();
           
           object.addProperty("persona", "Hola ! aqui me estas consultando");
           
                  
           return Response.ok(object.toString(), MediaType.APPLICATION_JSON)
                          .header("Access-Control-Allow-Origin", "*")
                          .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
                          .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With")
                          
                          .build();
        
    }
    
    @PUT
    @Path("actualizarp")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    
    public Response actualizarPersona() {
        
         JsonObject object = new JsonObject();
           
           object.addProperty("persona", "Hola ! aqui me estas actualizando");
           
                  
           return Response.ok(object.toString(), MediaType.APPLICATION_JSON)
                          .header("Access-Control-Allow-Origin", "*")
                          .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
                          .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With")
                          
                          .build();
        
    }
    
    @DELETE
    @Path("borrarp")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    
    public Response borrarPersona() {
        
         JsonObject object = new JsonObject();
           
           object.addProperty("persona", "Hola ! aqui me estas borrando");
           
                  
           return Response.ok(object.toString(), MediaType.APPLICATION_JSON)
                          .header("Access-Control-Allow-Origin", "*")
                          .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
                          .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With")
                          
                          .build();
        
    }
    
}
