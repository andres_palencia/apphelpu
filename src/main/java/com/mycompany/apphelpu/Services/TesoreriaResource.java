/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.apphelpu.Services;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.mycompany.apphelpu.DAO.ServicioDAO;
import com.mycompany.apphelpu.DAO.TesoreriaDAO;
import com.mycompany.apphelpu.Model.Servicio;
import com.mycompany.apphelpu.Model.Visita;
import com.mycompany.apphelpu.Util.Resultado;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Type;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * REST Web Service
 *
 * @author apalencia
 */
@Path("tesoreria")
public class TesoreriaResource {

    @Context
    private UriInfo context;
    
    TesoreriaDAO tesodao  = new TesoreriaDAO();
            JsonObject jobject = new JsonObject();
        Gson gson = new Gson();

    /**
     * Creates a new instance of TesoreriaResource
     */
    public TesoreriaResource() {
    }
    
          @GET
    @Path("historialtopes")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    
    public Response historialTopes() {
        

        List<Servicio> listartodoservicios =  tesodao.historialTopes();
        Type listType = new TypeToken<LinkedList<Servicio>>(){}.getType();

        String json = gson.toJson(listartodoservicios, listType);
        
       return Response.ok(json, MediaType.APPLICATION_JSON)
                          .header("Access-Control-Allow-Origin", "*")
                          .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
                          .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With")
                          
                          .build();
        
    }
    
        @GET
    @Path("listartopes/{usuario}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    
    public Response listarTopes(@PathParam("usuario") String usuario ) {
        

        List<Servicio> listartodoservicios =  tesodao.listTopes(usuario);
        Type listType = new TypeToken<LinkedList<Servicio>>(){}.getType();

        String json = gson.toJson(listartodoservicios, listType);
        
       return Response.ok(json, MediaType.APPLICATION_JSON)
                          .header("Access-Control-Allow-Origin", "*")
                          .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
                          .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With")
                          
                          .build();
        
    }
    
    @GET
        @Path("detalle_tope/{servicio}")
        @Consumes( MediaType.APPLICATION_JSON  )
        @Produces( MediaType.APPLICATION_JSON  )
        
        public Response detalleTope(@PathParam("servicio") String servicio ){

        List listasunto =  tesodao.detalleHistorialTope(servicio); // 
        
        System.out.println(""+listasunto);
        
        if ( !listasunto.get(5).toString().equalsIgnoreCase("A") ){
            
        
        jobject.addProperty("cedula_solicitante",        listasunto.get(0).toString());
        jobject.addProperty("servicio",         listasunto.get(1).toString());
        jobject.addProperty("punto_mvfijo",     listasunto.get(2).toString());
        jobject.addProperty("descripcion",      listasunto.get(3).toString());
        jobject.addProperty("direccion",        listasunto.get(4).toString());
        jobject.addProperty("estado",           listasunto.get(5).toString());
        jobject.addProperty("fecha",            listasunto.get(7).toString());
        jobject.addProperty("sucursal",              listasunto.get(6).toString());
        jobject.addProperty("id_asunto",        Integer.parseInt(listasunto.get(8).toString()));
        jobject.addProperty("asunto",           listasunto.get(9).toString());
        
        }else{
            
            try {
                validarIngreso(servicio);
            } catch (SQLException ex) {
                Logger.getLogger(ServicioResource.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
       
        
                    
            return  Response.ok(jobject.toString(), MediaType.APPLICATION_JSON)
                          .header("Access-Control-Allow-Origin", "*")
                          .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
                          .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With")
                          
                          .build();
            
        }
        
        @GET
        @Path("validaringresotecnico/{servicio}")
        @Consumes(MediaType.APPLICATION_JSON)
        @Produces(MediaType.APPLICATION_JSON)
        
        public Response validarIngreso(@PathParam("servicio") String servicio ) throws SQLException {
                           

            Visita visita = tesodao.cargarDatosDeVisita(servicio);
            
            
            if ( visita != null ) {
             
            jobject.addProperty("servicio", visita.getNum_servicio());
                jobject.addProperty("fecha", visita.getFecha_apertura());
                jobject.addProperty("sucursal", visita.getSucursal());
                jobject.addProperty("direccion", visita.getDireccion());
                jobject.addProperty("observacion", visita.getObs());
                jobject.addProperty("identificacion", visita.getIdentificacion_tecnico());
                jobject.addProperty("nombre_tecnico", visita.getNombre_tecnico());
                jobject.addProperty("asunto", visita.getAsunto());
                jobject.addProperty("foto", visita.getFoto());
                jobject.addProperty("estado", visita.getEstado());
                jobject.addProperty("cedula_solicitante", visita.getCedula_solicitante());
                
                
                                if ( visita.getEstado().equals("F")  ){
                                  
                jobject.addProperty("fecha_cierre", visita.getFecha_cierre().toString());
                jobject.addProperty("obs_cierre", visita.getObs_cierre());
                jobject.addProperty("pendiente", visita.getPendiente());
                jobject.addProperty("inventario", visita.getInventario());
                jobject.addProperty("imei", visita.getImei());
                jobject.addProperty("sim", visita.getSimcard());
                jobject.addProperty("operador", visita.getOperador());
                
                }
            
            }else{
                jobject.addProperty("codigo", 1);
                jobject.addProperty("error", "Evento de Aplicacion");
                jobject.addProperty("observacion", "Al parecer este servicio ya no esta asignado");
            }
            
           
            return Response.ok(jobject.toString(), MediaType.APPLICATION_JSON)
                          .header("Access-Control-Allow-Origin", "*")
                          .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
                          .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With")
                          
                          .build();
            
        
        }
        
        @GET
        @Path("listar_fecha_tope/{fechai}/{fechaf}")
        @Consumes(MediaType.APPLICATION_JSON)
        @Produces(MediaType.APPLICATION_JSON)
        
        public Response listarTopesPorFechas(@PathParam("fechai") String fechai, @PathParam("fechaf") String fechaf ) {
            
            System.out.println(""+fechai);
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            java.sql.Date fechaini = new java.sql.Date(Long.parseLong(fechai)); 
            java.sql.Date fechafin = new java.sql.Date(Long.parseLong(fechaf)); 
            System.out.println(""+fechaini);
            
            List<Servicio> lista_servicios = tesodao.listarServiciosPorFechaTeso(fechaini, fechafin);
            
            Type listType = new TypeToken<LinkedList<Servicio>>(){}.getType();

            String json = gson.toJson(lista_servicios, listType);
            
            
     
            return  Response.ok(json, MediaType.APPLICATION_JSON)
                          .header("Access-Control-Allow-Origin", "*")
                          .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
                          .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With")
                          
                          .build();
            
        }
        
        @GET
        @Path("validartopevisita/{numserv}/{tecnico}")
        @Consumes(MediaType.APPLICATION_JSON)
        @Produces(MediaType.APPLICATION_JSON)
        
        public Response validarTopesVisitaTecnica(@PathParam("numserv") String numserv, @PathParam("tecnico") String tecnico  ) throws SQLException{
  
            Resultado resultado = tesodao.genera_controlAcceso(tecnico,"");
            jobject.addProperty("codigo", 1);
         
            
            return  Response.ok(jobject.toString(), MediaType.APPLICATION_JSON)
                          .header("Access-Control-Allow-Origin", "*")
                          .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
                          .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With")
                          
                          .build();
            
            
            
        }
        
        
        @GET
        @Path("export_topes/detalle/{estado}")
        @Consumes({ MediaType.APPLICATION_JSON })
        @Produces("application/vnd.ms-excel")
        
        public Response exportaServicioExcelDetalle(@PathParam("estado") String estado ) throws IOException, InvalidFormatException{

            Workbook milibro = new XSSFWorkbook();          
            Sheet mihoja = milibro.createSheet("Servicios");

            List<Servicio> lista_estados_servicios =  tesodao.listaTopesByCriteria(2, estado);
            
            // Obtener la cantidad de filas de la consulta
            
            int rowCount = 0;
                
            for ( Servicio servicio : lista_estados_servicios ) {
                    
                Row fila = mihoja.createRow(++rowCount);
                            
                for ( int j = 1; j <= fila.getLastCellNum(); j ++ ) {
                            
                    Cell celda = fila.createCell(j);
                    celda.setCellValue("hajhahaha");
                    celda.setCellValue(servicio.getDescripcion());
                    
                }
                
            }
            
            String directory = System.getProperty("user.home");

            String fileLocation = directory + "/temp.xls";
            File file = new File(fileLocation);
            
            FileOutputStream outputStream = new FileOutputStream(fileLocation);
            milibro.write(outputStream);
            milibro.close();
            outputStream.flush();
            return  Response.ok((Object)file)
                                
                            .header("Access-Control-Allow-Origin", "*")
                            .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
                            .header("Content-Disposition","attachment; filename=export-excel"+System.currentTimeMillis()+".xls")
                            .build();

            
            
        
            
        }
        
        
        ////// Detalle historial mis solicitudes todos los estados
        
        
        @GET
        @Path("detalle_mis_topes/{servicio}")
        @Consumes( MediaType.APPLICATION_JSON  )
        @Produces( MediaType.APPLICATION_JSON  )
        
        public Response DetalleHistorialMisSolicitudes(@PathParam("servicio") String servicio ){

        List listasunto =  tesodao.detalleHistorialTope(servicio); // 
        
        if ( !listasunto.get(5).toString().equalsIgnoreCase("A") ){
        
     jobject.addProperty("documento",        listasunto.get(0).toString());
        jobject.addProperty("servicio",         listasunto.get(1).toString());
        jobject.addProperty("punto_mvfijo",     listasunto.get(2).toString());
        jobject.addProperty("descripcion",      listasunto.get(3).toString());
        jobject.addProperty("direccion",        listasunto.get(4).toString());
        jobject.addProperty("estado",           listasunto.get(5).toString());
        jobject.addProperty("fecha",            listasunto.get(7).toString());
        jobject.addProperty("sucursal",              listasunto.get(6).toString());
        jobject.addProperty("id_asunto",           Integer.parseInt(listasunto.get(8).toString()));
        jobject.addProperty("asunto",              listasunto.get(9).toString());
        
        }else{
            
            try {
                historialIngresoMisSolicitudes(servicio);
            } catch (SQLException ex) {
                Logger.getLogger(ServicioResource.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
       
        
                    
            return  Response.ok(jobject.toString(), MediaType.APPLICATION_JSON)
                          .header("Access-Control-Allow-Origin", "*")
                          .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
                          .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With")
                          
                          .build();
            
        }
        
        
        @GET
        @Path("historial_ingreso_missolicitudes/{servicio}")
        @Consumes(MediaType.APPLICATION_JSON)
        @Produces(MediaType.APPLICATION_JSON)
        
        public Response historialIngresoMisSolicitudes(@PathParam("servicio") String servicio ) throws SQLException {
                           

            Visita visita = tesodao.cargarDatosMisSolicitudesDeVisita(servicio);
            
            
            if ( visita != null ) {
             
            jobject.addProperty("servicio", visita.getNum_servicio());
            jobject.addProperty("fecha", visita.getFecha_apertura());
            jobject.addProperty("sucursal", visita.getSucursal());
            jobject.addProperty("direccion", visita.getDireccion());
            jobject.addProperty("observacion", visita.getObs());
            jobject.addProperty("identificacion", visita.getIdentificacion_tecnico());
            jobject.addProperty("nombre_tecnico", visita.getNombre_tecnico());
            jobject.addProperty("asunto", visita.getAsunto());
            jobject.addProperty("foto", visita.getFoto());
            jobject.addProperty("estado", visita.getEstado());
            jobject.addProperty("cedula_solicitante", visita.getCedula_solicitante());
            
                            if ( visita.getEstado().equals("F")  ){
                                  
                jobject.addProperty("fecha_cierre", visita.getFecha_cierre().toString());
                jobject.addProperty("obs_cierre", visita.getObs_cierre());
                jobject.addProperty("pendiente", visita.getPendiente());
                jobject.addProperty("inventario", visita.getInventario());
                jobject.addProperty("imei", visita.getImei());
                jobject.addProperty("sim", visita.getSimcard());
                jobject.addProperty("operador", visita.getOperador());
                
                }
            
            }else{
                jobject.addProperty("codigo", 1);
                jobject.addProperty("error", "Evento de Aplicacion");
                jobject.addProperty("observacion", "Al parecer este servicio ya no esta asignado");
            }
            
           
            return Response.ok(jobject.toString(), MediaType.APPLICATION_JSON)
                          .header("Access-Control-Allow-Origin", "*")
                          .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
                          .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With")
                          
                          .build();
            
        
        }
        
        
  
        
        

    /**
     * Retrieves representation of an instance of com.mycompany.apphelpu.Services.TesoreriaResource
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_XML)
    public String getXml() {
        //TODO return proper representation object
        throw new UnsupportedOperationException();
    }

    /**
     * PUT method for updating or creating an instance of TesoreriaResource
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.APPLICATION_XML)
    public void putXml(String content) {
    }
    
    
    @GET
    @Path("listarbycriterio_seguridad/{oper}/{dato}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    
    public Response listarServicioByCriterioSeguridad( @PathParam("oper") int oper, @PathParam("dato") String dato ) {
        

        List<Servicio> listasunto =  tesodao.listServiciosByCriteriaSeguridad(oper,dato);
        Type listType = new TypeToken<LinkedList<Servicio>>(){}.getType();

        String json = gson.toJson(listasunto, listType);
        
       return Response.ok(json, MediaType.APPLICATION_JSON)
                          .header("Access-Control-Allow-Origin", "*")
                          .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
                          .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With")
                          
                          .build();
        
    }
    
    
    @POST
        @Path("cerrar_servicio_seg")
        @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_FORM_URLENCODED})
        @Produces(MediaType.APPLICATION_JSON)
        
        public Response cerrarServicioSeguridad(
                
                @FormParam("servicio") String servicio, 
                @FormParam("usuario") String usuario, 
                @FormParam("descripcion") String descripcion, 
                @FormParam("estado_serv") String estado_serv, 
                @FormParam("pendiente") String pendiente, 
                @FormParam("inventario") String inventario, 
                @FormParam("imei") String imei, 
                @FormParam("sim") String sim, 
                @FormParam("operador") String operador,
                @FormParam("obs_cierre") String cierre
                ) throws SQLException {

            
                Resultado cerrar_servicio =  tesodao.cerrarServicio(servicio, Integer.parseInt(usuario), descripcion,estado_serv, pendiente, Integer.parseInt(inventario), imei,sim, Integer.parseInt(operador),cierre); // 55 n 82 21 2 piso
                jobject.addProperty("codigo", cerrar_servicio.getCodigo());
                jobject.addProperty("estado", cerrar_servicio.getEstado());
                jobject.addProperty("resultado", cerrar_servicio.getResultado());
            
                return  Response.ok(jobject.toString(), MediaType.APPLICATION_JSON)
                          .header("Access-Control-Allow-Origin", "*")
                          .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
                          .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With")
                          
                          .build();
            
            
        }
        
    
    
    
    
}
